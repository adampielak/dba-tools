#!/usr/bin/env bash

source config/providers/alerts/email.sh

ALERT_MESSAGE="${1}"

if [[ ${EMAIL} -eq 1 ]]; then
    if [[ ${#EMAIL_LIST[@]} -gt 0 ]]; then
        echo "${ALERT_MESSAGE}" | mailx -s "[ALERT] ${TOOL_NAME} on ${HOSTNAME} returned an error!" "${EMAIL_LIST[@]}"
    else
        logger "No alert was sent, EMAIL_LIST appears to be empty. Please set it in ${BASE_DIR}/config/providers/alerts/email.sh ..."
    fi
fi