logger() {

    local MESSAGE=${1}
    local LOG_DATE_FORMAT=$( date "+%F %H:%M:%S")

    if [[ ! -e "${LOG_DIR}" ]]; then
        mkdir -p "${LOG_DIR}"
    fi

    if [[ "${VERBOSE}" -eq 1 ]]; then
        echo "${MESSAGE}"
    fi

    if [[ -z "${LOG_FILE}" ]]; then
        echo "Log file not specified. Defaulting to /tmp/${TOOL_NAME}.log"
        LOG_FILE="/tmp/${TOOL_NAME}.log"
    fi

    if [[ -w "${LOG_DIR}" ]]; then
        echo "[${LOG_DATE_FORMAT}] ${MESSAGE}" >> ${LOG_FILE}
    fi

}