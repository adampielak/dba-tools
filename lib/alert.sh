#!/usr/bin/env bash

source config/base_config.sh

# Runs providers to handle alerting

# Alerting enabled? If not, simply do nothing.
if [[ $ALERTING -eq 1 ]]; then

    # Cleanup alert locks > ALERT_SLEEP_MINUTES
    find /tmp -name '*.alert.lock' -mmin $((${ALERT_SLEEP_MINUTES} - 1)) -delete

    # Alerting is enabled, is STOP_ALERT_SPAM on AND an alert lock file exists?
    if [[ ${STOP_ALERT_SPAM} -eq 1 ]] && [[ -f "/tmp/${TOOL_NAME}.alert.lock" ]]; then
        logger "An alert was sent in the last ${ALERT_SLEEP_MINUTES} minute(s) ..."
        logger "You can manually clear this with the following command:"
        logger "  rm -f /tmp/${TOOL_NAME}.alert.lock"
    else
        # Touch an alert lock file whether STOP_ALERT_SPAM is on or not.
        touch "/tmp/${TOOL_NAME}.alert.lock"

        # Nothing to stop us from alerting now!
        # Step through all scripts found in lib/providers/alerts
        for PROVIDER in $( find "${BASE_DIR}/lib/providers/alerts" -name '*.sh' )
        do
            source "${PROVIDER}"
        done
    fi
fi