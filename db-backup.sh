#!/usr/bin/env bash

cd "${BASE_DIR}" || exit

## Import required config / libraries
## Local includes
source config/db_backup.sh
source config/base_config.sh

# Util library includes.
source lib/logger.sh

logger "Starting backup script ..."

# Some sanity checks
if [[ -z "${DB_USER}" ]]; then
    logger "DB_USER is not set. Check ${BASE_DIR}/config/base_config.sh"
    exit 1
fi

if [[ -z "${BACKUPDIR}" ]] ; then
    logger "Backup directory isn't set. Check ${BASE_DIR}/config/base_config.sh"
    exit 1
fi

if [[ -z "${KEEP_DAYS}" ]]; then
    KEEP_DAYS=3
fi

DB_OPTS=( "-u${DB_USER}" )

if [[ -n "${DB_PASS}" ]]; then
    DB_OPTS+=( "-p${DB_PASS}" )
fi

logger "Basic checks passed ..."

BACKUPDIR_TODAY="${BACKUPDIR}/backup_${DATE}"

# Figure out if we're going to use mariabackup or xtrabackup.. if not already set.
#
# In general, if you are running MariaDB, you should be using an equal or higher version of Mariabackup.
# We simply assume the admin knows what should be installed.

# Find our BACKUPBIN based on installed distribution.
DBDIST=$( mysql --version | grep MariaDB )
if [[ -n "${DBDIST}" ]]; then
    logger "Selected mariabackup as the BACKUPBIN ..."
    BACKUPBIN=mariabackup
else
    logger "Selected xtrabackup as the BACKUPBIN ..."
    BACKUPBIN=xtrabackup
fi

# And now to fire off the backup process.
if [[ ${INCREMENTAL} -eq 1 ]]; then

    logger "Incremental backup process starting ..."

    BACKUP_OPTS=(
        "--backup"
        "--target-dir=${BACKUPDIR_TODAY}"
        "--stream=xbstream"
        "--extra-lsndir=${BACKUPDIR_TODAY}"
    )

    # Determine if compression is on
    if [[ ${COMPRESSION} -eq 1 ]]; then
        logger "Compression is enabled ..."
        BACKUP_OPTS+=( "--compress" )

        #Let the user know if qpress is found or not
        if [[ ! -n $( command -v qpress ) ]]; then
            logger "NOTE: You do not appear have qpress installed. You will not be able to decompress the incremental backups."
        fi
    fi

    # Determine if parallel is on
    if [[ ${PARALLEL} -eq 1 ]]; then
        logger "Parallel compression is enabled in the config ..."

        # Make sure we have nproc, even though there are other ways.
        if [[ -n $( command -v nproc ) ]]; then
            NPROC=$( echo "$( nproc --all ) / 2" | bc | awk '{print int($1+0.5)}' )
            logger "Setting --parallel and --compress-threads to ${NPROC} ( Total cores / 2, rounded up )"
            BACKUP_OPTS+=(
                "--parallel=${NPROC}"
                "--compress-threads=${NPROC}"
            )
        else
            logger "nproc command not found, parallel compression will be disabled ..."
        fi
    fi

    BACKUP_TYPE='full'

    # Is there a checkpoints file? Incremental time.
    if [[ -f "${BACKUPDIR_TODAY}/xtrabackup_checkpoints" ]]; then
        BACKUP_TYPE='incremental'
        LSN=$( awk '/to_lsn/ {print $3;}' "${BACKUPDIR_TODAY}/xtrabackup_checkpoints" )
        BACKUP_OPTS+=( "--incremental-lsn=${LSN}" )
    fi

    logger "Backup type: ${BACKUP_TYPE^} ..."

    # Run the backup process
    mkdir -p "${BACKUPDIR_TODAY}"
    find "${BACKUPDIR_TODAY}" -type f -name '*.incomplete' -delete
    "${BACKUPBIN}" "${DB_OPTS[@]}" "${BACKUP_OPTS[@]}" > "${BACKUPDIR_TODAY}/${BACKUP_TYPE}-${DATE}-${TIMESTAMP}.xbstream.incomplete" 2> "${LOG_DIR}/backup_${DATE}.log"
    mv "${BACKUPDIR_TODAY}/${BACKUP_TYPE}-${DATE}-${TIMESTAMP}.xbstream.incomplete" "${BACKUPDIR_TODAY}/${BACKUP_TYPE}-${DATE}-${TIMESTAMP}.xbstream"

    logger "Backup process completed ..."

else

    # Check if a full backup already exists
    if [[ -d "${BACKUPDIR_TODAY}" ]]; then
        logger "Incremental backups are disabled and a directory for today exists. Backup process canceled."
        logger "If you intend to make a new backup, please remove or rename ${BACKUPDIR_TODAY}."
        exit 1
    fi

    # Run a simple, full backup.
    "${BACKUPBIN}" "${DB_OPTS[@]}" --backup --target-dir="${BACKUPDIR_TODAY}" 2> "${LOG_DIR}/backup_${DATE}.log"

    logger "Backup process completed ..."

fi

# Were we successful?
RES=$( grep 'completed OK' "${LOG_DIR}/backup_${DATE}.log" )
if [[ -z "${RES}" ]]; then
    # This is not POSIX compliant. Total BASHism, which is just fine.
    source lib/alert.sh "Backup failed."
else
    logger "Backup successful ..."
    logger "Cleaning old backups ..."

    # Clean up backups..should find another way to do this eventually.
    BACKUP_COUNT=$( ls "${BACKUPDIR}" | grep backup_ | wc -l )

    if [[ ${BACKUP_COUNT} -gt ${KEEP_DAYS} ]]; then
        TO_DELETE=$(( ${BACKUP_COUNT} - ${KEEP_DAYS} ))
        ls -r "${BACKUPDIR}" | tail -n ${TO_DELETE} | xargs rm -rf
    fi
fi

# FIN
logger "Script completed."
exit 0