#!/usr/bin/env bash

# This script is used to parse output from pt-heartbeat running on a slave.

## Import required info / config / libraries

## Local includes
source config/heartbeat_watcher.sh
source config/base_config.sh

# Util library includes.
source lib/logger.sh

cd "${BASE_DIR}" || exit

DB_OPTS=( "-u${DB_USER}" )

if [[ -n "${DB_PASS}" ]]; then
    DB_OPTS+=( "-p${DB_PASS}" )
fi

logger "Beginning check ... "

DBDIST=$( mysql --version | grep MariaDB )

if [[ -n "${DBDIST}" ]]; then
    SHOW_SLAVE="SHOW ALL SLAVES STATUS\G"
else
    SHOW_SLAVE="SHOW SLAVE STATUS\G"
fi

# Get the Master_Server_Id(s) to loop through
MASTER_ID=( $( mysql "${DB_OPTS[@]}" -e "${SHOW_SLAVE}\G" 2>&1 | grep -v "Using a password" | grep Master_Server_Id | awk '{print $2}' ) )

# Step through the found Id(s) and alert if any are > warn/crit seconds
for id in "${MASTER_ID[@]}"
do
    RES=$( pt-heartbeat --config=config/pt-heartbeat-watcher.conf --master-server-id ${MASTER_ID} | cut -d . -f 1 )
    if [[ "${RES}" -gt "${WARNING_LAG_SECONDS}" ]]; then
        if [[ "${RES}" -gt "${CRITICAL_LAG_SECONDS}" ]]; then
            logger "Replication state is critical for Master_Server_Id: ${id}. Alerting ..."
            exit 0
        fi
        logger "Replication is in warning state for Master_Server_Id: ${id}. Alerting ..."
        exit 0
    fi
    logger "Replication is healthy for Master_Server_Id: ${id}!"
done
