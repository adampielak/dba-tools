#!/usr/bin/env bash

# Start / Ensure pt-heartbeat is running

## Import required info / config / libraries
## Local includes
source config/heartbeat_daemon.sh
source config/base_config.sh

# Util library includes.
source lib/logger.sh

cd "${BASE_DIR}" || exit

RUNNING=$( ps -ef | grep pt-heartbeat | grep -v grep | grep pt-heartbeat-daemon )

if [[ -z "${RUNNING}" ]]; then
    logger "Heartbeat not running. Starting ... "
    pt-heartbeat --config=config/pt-heartbeat-daemon.conf
else
    logger "Heartbeat is running."
fi
