## Enables the slack alert provider
## Set to 0 to turn it off
SLACK=1

## Webhook URL
WEBHOOK_URL=""

## Channel name or channel ID
CHANNEL=""

## Username or hostname
USERNAME=""

## Icon emoji eg. ghost
ICON_EMOJI="ghost"

## good|warning|danger|#hex color
COLOR=""

## Author name
AUTHORNAME=""

## Title text
TITLE=""

## Title link URL
TITLE_LINK=""

## Footer text
FOOTER=""
