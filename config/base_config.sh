# This file must be loaded AFTER the config file for the script you are running.
# Without it, TOOL_NAME isn't set.

# Set your local DB User and Password
DB_USER='root'
DB_PASS=''


##
## Alerting Config
##

# Enable/Disable alerting
ALERTING=0

# Stop alert spam if a check/scripts runs often. It's best to leave this on.
# ALERT_SLEEP_MINUTES controls how long it "sleeps" for.
STOP_ALERT_SPAM=1

# How long to sleep between alerts in minutes
ALERT_SLEEP_MINUTES=30


##
## Backup / extract / prepare config
##

# Full path to your backup directory.
BACKUPDIR=''

# Full path where you want the backup rebuild done
# By default: ${BACKUPDIR}/restore
RESTOREDIR="${BACKUPDIR}/restore"

# Enable or disable incremental backups
INCREMENTAL=0

# Use Compression - On by default, only used if incrementals are enabled.
COMPRESSION=1

# Use parallel compression, only used if incrementals are enabled.
PARALLEL=1

# Number of days of backups to keep
KEEP_DAYS=3


##
## Max Connection Watcher Config
##

# Percent expressed as decimal
MAX_CONN_ALLOWED_PCT="0.80"

##
## Heartbeat watcher config (replication lag)
WARNING_LAG_SECONDS=300
CRITICAL_LAG_SECONDS=900

##
# Things-you-shouldn't-touch Config
##

BASE_DIR=$( dirname "$( readlink -f $0 )" )

DATE=$( date "+%F" )
TIMESTAMP=$( date +%s )

HOSTNAME=$( hostname -s )
HOSTNAME_FULL=$( hostname -a )

# Set our logging directory
LOG_DIR="${BASE_DIR}/logs/${TOOL_NAME}"
LOG_FILE="${LOG_DIR}/${DATE}.log"